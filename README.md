# webchat-embed

A JS script to embed a [Wikit](https://wikit.ai) webchat into a website.

## Getting started

### Installation

- Clone the repo `git clone https://gitlab.com/wikit-ai/webchat-embed.git`.
- Open the file `example.html` in your browser.

### Usage

The script comes with parameters to identify the user and to cutomize the embed chat window.

|                 Property              |     Type      |      Default     |                    Description                           |
|---------------------------------------|---------------|------------------|----------------------------------------------------------|
| webchatParams                         | `object`      |  /               | The url params to identify the user.                     |
| webchatParams.userId                  | `string`      |  /               | the id of the user.                                      |
| webchatParams.userIdType              | `'email'/'login'` |  /            | The type of id.                                          |
| webchatParams.userFirstName           | `string`      |  /               | The first name of the user.                              |
| webchatParams.userLastName            | `string`      |  /               | The last name of the user                                |
| webchatParams.webChatToken            | `string`      |  /               | The Wikit webchat token.                                 |
| color                                 | `string`             | `'#243469'`      | The color of the webchat button and of the header. |
| height                                | `string`             | `'70%'`          | The height of the chat window. |
| width                                 | `string`             | `'25%'`          | The width of the chat window. |
| chatButtonIcon                        | `object`             |        /    | Customize the icon of the chat button. |
| chatButtonIcon.url                    | `string`             | /      | The url of the chat button icon. |
| chatButtonIcon.altText                | `string`             | / | The alternative text of the chat button icon. |
| chatButtonIcon.width                  | `string`             | `'50px'` | The width of the chat button icon. |
| chatButtonIcon.height                 | `string`             | `'50px'` | The height of the chat button icon. |
| chatButtonIcon.borderRadius           | `string`                    | `'50%'` | The border radius of the chat button icon. |
| chatButtonTooltip                     | `object`                    |        /    | The chat button tooltip description. |
| chatButtonTooltip.text                | `string`                    | / | The chat button tooltip text. |
| chatButtonTooltip.backgroundColor     | `string`                    | `'#9e9e9e'` | The chat button tooltip background color. |
| chatButtonTooltip.textColor           | `string`                    | `'#fff'` | The chat button tooltip text color. |
| chatButtonTooltip.visibility          | `'visible'/'hidden'` | `'hidden'`  | The chat button tooltip visibility. |
| headerButtons                         | `object`             |        /    | Customize the buttons of the webchat window header. |
| headerButtons.color                   | `string`             | `'#fff'` | The color of the webchat window header. |
| headerButtons.closeIconDescription    | `string`             | `'Fermer la fenêtre'` | The aria label of the close icon button. |
| headerButtons.launchIconDescription   | `string`             | `'Ouvrir dans un nouvel onglet'` | The aria label of the launch icon button. |
| headerButtons.maximizeIconDescription | `string`             | `'Agrandir la fenêtre'` | The aria label of the maximize icon button. |
| headerButtons.minimizeIconDescription | `string`             | `'Rétrécir la fenêtre'` | The aria label of the minimize icon button. |
| opening                               | `object`             |        /    | The opening of the chat window after page load. |
| opening.mode                          | `string`             | `'open'|'delay'|'close'` | The opening mode. |
| opening.delay                         | `string`             | / | For a delay mode, the delay in milliseconds to open the window. |
| opening.memorize                      | `boolean`            | `true` | Memorize the state of opening. |
| position                              | `object`             |        /    | The position of the chat button on the document. |
| position.right                        | `string`             | `'1rem'` | The position of the chat button. |
| position.left                         | `string`             | `'1rem'` | The position of the chat button. |


## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are greatly appreciated.

- Fork the Project
- Create your Feature Branch `git checkout -b feat/amazing-feature`
- Commit your Changes `git commit -m 'Add some Amazing Feature'`
- Push to the Branch `git push origin feat/amazing-feature`
- Open a Merge Request

## License

Distributed under the GNU GPLv3 License. See [LICENSE](LICENSE) for more information.
