/**
 * Embed code for Wikit Webchat.
 */

const chatButtonLabel = 'Bouton de chat';
const launchDefaultLabel = 'Ouvrir dans un nouvel onglet';
const minimizeDefaultLabel = 'Rétrécir la fenêtre';
const maximizeDefaultLabel = 'Agrandir la fenêtre';
const closeDefaultLabel = 'Fermer la fenêtre';

/**
 * Encode webchat params to get the iframe url.
 * @param {Object} webchatParams
 * - @param {String} webchatParams.userId the id of the user.
 * - @param {String} webchatParams.userIdType the type of id. Can be email or login.
 * - @param {String} webchatParams.webChatToken the webchat token.
 * - @param {String} webchatParams.userFirstName the first name of the user.
 * - @param {String} webchatParams.userLastName the last name of the user.
 * @returns {String} The url of the webchat.
 */
function getUrl(webchatParams) {
    let url = 'https://wikit-webchat-v2.eu-de.mybluemix.net/webchat/';

    if (!webchatParams) {
        console.error('webchatParams is required');
    }

    if (!webchatParams.userId) {
        console.error('userId is missing in webchatParams');
    }

    if (!webchatParams.userIdType) {
        console.error('userIdType is missing in webchatParams');
    }

    if (!webchatParams.webChatToken) {
        console.error('webChatToken is missing in webchatParams');
    }

    if (webchatParams.userId && webchatParams.userIdType && webchatParams.webChatToken) {
        const encodedParam = window.btoa(JSON.stringify(webchatParams));
        url = `${url}${encodedParam}`;
    }

    return url;
}

/**
 * Add the project style sheet dynamically.
 */
function addStylesheetRules() {
    const style = document.createElement('style');
    document.head.appendChild(style);
    const stylesheet = style.sheet;

    // Root variables
    stylesheet.insertRule(
        `:root {
            --webchat-button-width: 50px;
            --webchat-button-height: 50px;
            --webchat-button-border-radius: 50%;
            --main-color: #243469;
            --window-height: 70%;
            --window-width: 25%;
            --tooltip-background-color: #9e9e9e;
            --tooltip-text-color: #fff;
            --header-icon-color: #fff;
            --tooltip-visibility: hidden;
            --tooltip-opacity: 0;
            --bottom: 1rem;
            --right: 1rem;
        }`,
        0,
    );

    // Global container
    stylesheet.insertRule(
        `#botDiv {'
            width: 0;
            height: 0;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
        }`,
        0,
    );

    // Speech bubble button
    stylesheet.insertRule(
        `.webchat__btn {
            z-index: 10000;
            background-color: var(--main-color);
            border: none;
            cursor: pointer;
            position: fixed;
            bottom: var(--bottom);
            right: var(--right);
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__btn--visible-on-page-load {
            width: var(--webchat-button-width);
            height: var(--webchat-button-height);
            border-radius: var(--webchat-button-border-radius);
            visibility: visible;
            opacity: 1;
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__btn--visible {
            visibility: visible;
            opacity: 1;
            width: var(--webchat-button-width);
            height: var(--webchat-button-height);
            border-radius: var(--webchat-button-border-radius);
            animation: button-show 0.25s ease-out;
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__btn--hidden {
            visibility: hidden;
            opacity: 0;
            width: 0;
            height: 0;
            border-radius: 0.5rem;
            animation: button-hide 0.5s ease-out;
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__btn__tooltip-text {
            visibility: var(--tooltip-visibility);
            width: max-content;
            min-width: 120px;
            max-width: 13rem;
            padding: 5px 10px;
            background-color: var(--tooltip-background-color);
            color: var(--tooltip-text-color);
            text-align: center;
            border-radius: 6px;
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            z-index: 1;
            top: 50%;
            -webkit-transform: translate(-100%, -50%);
            transform: translate(-100%, -50%);
            left: -10px;
            opacity: var(--tooltip-opacity);
            transition: all .7s ease;
        }`, 0,
    );

    stylesheet.insertRule(
        `.webchat__btn__tooltip-text::after {
            content: "";
            position: absolute;
            top: 50%;
            left: 100%;
            margin-top: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: transparent transparent transparent var(--tooltip-background-color);
        }`, 0,
    );

    stylesheet.insertRule(
        `.webchat__btn:hover .webchat__btn__tooltip-text, .webchat__btn:focus .webchat__btn__tooltip-text {
            opacity: 1;
            visibility: visible;
        }`, 0,
    );

    stylesheet.insertRule(
        `.webchat__btn--custom-img {
            width: 100%;
            height: 100%;
            background-position: center;
            background-size: cover;
            flex-shrink: 0;
            border-radius: var(--webchat-button-border-radius);
        }`, 0,
    );

    stylesheet.insertRule(
        `.speech-bubble {
            position: relative;
            background-color: white;
            border-radius: 7px;
            width: 30px;
            height: 20px;
            width: 60%;
            height: 40%;
            margin-left: auto;
            margin-right: auto;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }`, 0,
    );
    stylesheet.insertRule(
        `.speech-bubble:after {
            content: "";
            position: absolute;
            bottom: 0;
            left: 50%;
            width: 0;
            height: 0;
            border: 15px solid transparent;
            border-top-color: white;
            border-bottom: 0;
            border-left: 0;
            margin-left: -5px;
            margin-bottom: -8px;
        }`, 0,
    );

    stylesheet.insertRule(
        `.sr-only {
            position: absolute;
            width: 1px;
            height: 1px;
            padding: 0;
            margin: -1px;
            overflow: hidden;
            clip: rect(0, 0, 0, 0);
            border: 0;
        }`, 0,
    );

    // media
    stylesheet.insertRule(
        `@media (min-width: 320px) and (max-width: 1024px) {
            .webchat__container--opened {
                width: 100%;
                height: 100%;
                bottom: 0;
                right: 0;
                animation: iframe-open-mobile 0.3s ease-out;
            }
            #webchat__header__btn--max, #webchat__header__btn--launch, #webchat__header__btn--reduce {
                display: none;
            }
            #webchat__header {
                justify-content: flex-start;
            }
        }`, 0,
    );

    stylesheet.insertRule(
        `@media (min-width: 1224px) {
            .webchat__container--opened {
                width: var(--window-width);
                height: var(--window-height);
                bottom: var(--bottom);
                right: var(--right);
            }
            #webchat__header__btn--max, #webchat__header__btn--launch, #webchat__header__btn--reduce {
                display: initial;
            }
            #webchat__header {
                justify-content: flex-end;
            }
        }`, 0,
    );

    // Webchat window container
    stylesheet.insertRule(
        `#webchat__container {
            position: fixed;
            box-shadow: 0.3px 4px 8px 0 rgba(66, 66, 66, 0.4);
            border-radius: 0.2rem;
            z-index: 100000000;
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__container--hidden {
            width: 0;
            height: 0;
            opacity: 0;
            visibility: hidden;
            animation: iframe-close 0.25s ease-out;
            bottom: var(--bottom);
            right: var(--right);
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__container--hidden-on-page-load {
            width: 0; height: 0;
            opacity: 0;
            visibility: hidden;
            bottom: var(--bottom);
            right: var(--right);
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__container--opened {
            z-index: 10000;
            opacity: 1;
            visibility: visible;
            width: var(--window-width);
            height: 70%;
            bottom: var(--bottom);
            right: var(--right);
            animation: iframe-open 0.3s ease-out;
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__container--full-screen {
            width: 100%;
            height: 100%;
            bottom: 0;
            right: 0;
            animation: iframe-maximize 0.5s ease-out;
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__container--closed-full-screen {
            opacity: 0;
            visibility: hidden;
            height: 0;
            width: 0;
            animation: iframe-close-full-screen 0.3s ease-out;
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__container--minimized {
            width: var(--window-width);
            height: var(--window-height);
            bottom: var(--bottom);
            right: var(--right);
            animation: iframe-minimize 0.25s ease-out;
        }`, 0,
    );

    // Webchat window header
    stylesheet.insertRule(
        `#webchat__header {
            display: flex;
            justify-content: flex-end;
            background-color: var(--main-color);
            border-radius: 0.2rem 0.2rem 0 0;
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__header__btn {
            border: none;
            background-color: transparent;
            padding: 8px;
            cursor: pointer;
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__header__btn:hover {
            background-color: rgba(0, 0, 0, 0.2);
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__header__btn:focus {
            outline: none;
            background-color: rgba(0, 0, 0, 0.2);
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__header__btn svg {
            width: 16px;
            height: 16px;
            fill: var(--header-icon-color);
        }`, 0,
    );
    stylesheet.insertRule(
        `.webchat__header__btn--hidden {
            display: none !important;
        }`, 0,
    );
    stylesheet.insertRule(
        `#webchat__header__btn--close .cls-1 {
            fill: var(--header-icon-color);
        }`, 0,
    );

    // Webchat window content & iframe
    stylesheet.insertRule(
        `#webchat__content {
            width: 100%;
            height: 80%;
        }`, 0,
    );
    stylesheet.insertRule(
        `#webchat__iframe {
            border: 0;
            position: absolute;
            top: 30px;
            left: 0;
            width: 100%;
            height: calc(100% - 30px);
            border-radius: 0 0 0.2rem 0.2rem;
        }`, 0,
    );

    // keyframes
    stylesheet.insertRule(
        `@keyframes iframe-maximize {
            from {
                width: var(--window-width);
                height: var(--window-height);
                bottom: var(--bottom);
                right: var(--right);
            }
            to {
                width: 100%;
                height: 100%;
                bottom: 0;
                right: 0;
            }
        }`, 0,
    );
    stylesheet.insertRule(
        `@keyframes iframe-minimize {
            from {
                width: 100%;
                height: 100%;
                bottom: 0;
                right: 0;
            }
            to {
                width: var(--window-width);
                height: var(--window-height);
                bottom: var(--bottom);
                right: var(--right);
            }
        }`, 0,
    );
    stylesheet.insertRule(
        `@keyframes button-hide {
            from {
                opacity: 1;
                visibility: visible;
                border-radius: var(--webchat-button-border-radius);
                width: var(--webchat-button-width);
                height: var(--webchat-button-height);
            }
            to {
                opacity: 0;
                visibility: hidden;
                border-radius: 0.2rem;
                width: var(--window-width);
                height: var(--window-height);
            }
        }`, 0,
    );
    stylesheet.insertRule(
        `@keyframes button-show {
            from {
                opacity: 0;
                visibility: hidden;
                border-radius: 0.2rem;
                width: var(--window-width);
                height: var(--window-height);
            }
            to {
                opacity: 1;
                visibility: visible;
                border-radius: var(--webchat-button-border-radius);
                width: var(--webchat-button-width);
                height: var(--webchat-button-height);
            }
        }`, 0,
    );
    stylesheet.insertRule(
        `@keyframes iframe-open {
            from {
                opacity: 0;
                visibility: hidden;
                height: 0;
                width: 0;
            }
            to {
                opacity: 1;
                visibility: visible;
                height: var(--window-height);
                width: var(--window-width);
            }
        }`, 0,
    );
    stylesheet.insertRule(
        `@keyframes iframe-open-mobile {
            from {
                opacity: 0;
                visibility: hidden;
                height: 0;
                width: 0;
            }
            to {
                opacity: 1;
                visibility: visible;
                height: 100%;
                width: 100%;
            }
        }`, 0,
    );
    stylesheet.insertRule(
        `@keyframes iframe-close {
            from {
                opacity: 1;
                visibility: visible;
                height: var(--window-height);
                width: var(--window-width);
            }
            to {
                opacity: 0;
                visibility: hidden;
                height: 0;
                width: 0;
            }
        }`, 0,
    );
    stylesheet.insertRule(
        `@keyframes iframe-close-full-screen {
            from {
                opacity: 1;
                visibility: visible;
                height: 100%;
                width: 100%;
                bottom: 0;
                right: 0;
            }
            to {
                opacity: 0;
                visibility: hidden;
                height: 0;
                width: 0;
                bottom: var(--bottom);
                right: var(--right);
            }
        }`, 0,
    );
}

/**
 * Customize embedded webchat CSS style.
 * @param {Object} params
 * @param {Object} params.chatButtonIcon The chat button params.
 *  - @param {String} params.chatButtonIcon.height The height of the chat button icon.
 *  - @param {String} params.chatButtonIcon.width The width of the chat button icon.
 *  - @param {String} params.chatButtonIcon.borderRadius The border radius of the chat button icon.
 * @param {String} params.chatButtonTooltip The chat button tooltip description.
 *  - @param {String} params.chatButtonTooltip.backgroundColor The chat button tooltip background color.
 *  - @param {String} params.chatButtonTooltip.textColor The chat button tooltip text color.
 * @param {String} params.color The color of the speech bubble button and of the header.
 * @param {Object} param.headerButtons Customize header buttons.
 * @param {Object} param.headerButtons.color The color of the header icon buttons.
 * @param {String} params.height The height of the chat window.
 */
function customizeStyle({chatButtonIcon, chatButtonTooltip, color, headerButtons, height, position, width}) {
    const bodyStyle = document.body.style;

    if (chatButtonIcon) {
        if (chatButtonIcon.height) {
            bodyStyle.setProperty('--webchat-button-height', chatButtonIcon.height);
        }

        if (chatButtonIcon.width) {
            bodyStyle.setProperty('--webchat-button-width', chatButtonIcon.width);
        }

        if (chatButtonIcon.borderRadius) {
            bodyStyle.setProperty('--webchat-button-border-radius', chatButtonIcon.borderRadius);
        }
    }

    if (color) {
        bodyStyle.setProperty('--main-color', color);
    }

    if (height) {
        bodyStyle.setProperty('--window-height', height);
    }

    if (width) {
        bodyStyle.setProperty('--window-width', width);
    }

    if (chatButtonTooltip) {
        if (chatButtonTooltip.backgroundColor) {
            bodyStyle.setProperty('--tooltip-background-color', chatButtonTooltip.backgroundColor);
        }

        if (chatButtonTooltip.textColor) {
            bodyStyle.setProperty('--tooltip-text-color', chatButtonTooltip.textColor);
        }

        if (chatButtonTooltip.visibility) {
            bodyStyle.setProperty('--tooltip-visibility', chatButtonTooltip.visibility);

            if (chatButtonTooltip.visibility === 'visible') {
                bodyStyle.setProperty('--tooltip-opacity', 1);
            }
        }
    }

    if (headerButtons && headerButtons.color) {
        bodyStyle.setProperty('--header-icon-color', headerButtons.color);
    }

    if (position) {
        if (position.bottom) {
            bodyStyle.setProperty('--bottom', position.bottom);
        }

        if (position.right) {
            bodyStyle.setProperty('--right', position.right);
        }
    }
}

/**
 * Get chat button HTML markup in function of customization parameters
 * @param {Object} params
 * @param {Object} params.chatButtonIcon Customize the icon of the chat button.
 *  - @param {String} params.chatButtonIcon.url The url of the chat button icon.
 *  - @param {String} params.chatButtonIcon.altText The alternative text of the chat button icon.
 * @param {String} params.chatButtonTooltip The chat button tooltip description.
 *  - @param {String} params.chatButtonTooltip.text The chat button tooltip text.
 * @returns {String} The chat button markup.
 */
function getChatButtonMarkup({chatButtonIcon, chatButtonTooltip}) {
    let chatIcon = '<div class="speech-bubble"></div>';
    let tooltip = '';
    let webchatButtonAriaAttributes = 'role="button" tabindex="0"';

    if (chatButtonIcon && chatButtonIcon.url) {
        const altText = chatButtonIcon.altText ? chatButtonIcon.altText : chatButtonLabel;
        chatIcon = `
            <div
                class="webchat__btn--custom-img"
                style="background-image: url(${chatButtonIcon.url})"
                role="img"
                aria-label="${altText}"
            ></div>`;
    }

    if (chatButtonTooltip) {
        if (chatButtonTooltip.text) {
            tooltip = `<span class="webchat__btn__tooltip-text" role="tooltip" id="webchat-btn-tooltip">
                ${chatButtonTooltip.text}</span>`;
            webchatButtonAriaAttributes = `${webchatButtonAriaAttributes} aria-describedby="webchat-btn-tooltip"`;
        }
    }

    return `<div class="webchat__btn webchat__btn--visible-on-page-load" ${webchatButtonAriaAttributes}>${chatIcon}${tooltip}</div>`;
}

/**
 * Get the HTML markup of the webchat window header.
 * @param {Object} headerButtons Customize the buttons of the webchat window header.
 *  - @param {String} headerButtons.closeIconDescription Customize the aria label of the close icon button.
 *  - @param {String} headerButtons.launchIconDescription Customize the aria label of the launch icon button.
 *  - @param {String} headerButtons.maximizeIconDescription Customize the aria label of the maximize icon button.
 *  - @param {String} headerButtons.minimizeIconDescription Customize the aria label of the minimize icon button.
 * @returns {String} The header markup.
 */
function getHeaderMarkup(headerButtons) {
    const getButtonAriaAttributes = description => `aria-label= "${description}"`;
    const iconAriaAttributes = 'aria-hidden="true" focusable="false"';

    let launchLabel = getButtonAriaAttributes(launchDefaultLabel);
    let minimizeLabel = getButtonAriaAttributes(minimizeDefaultLabel);
    let maximizeLabel = getButtonAriaAttributes(maximizeDefaultLabel);
    let closeLabel = getButtonAriaAttributes(closeDefaultLabel);

    let launchTitle = launchDefaultLabel;
    let minimizeTitle = minimizeDefaultLabel;
    let maximizeTitle = maximizeDefaultLabel;
    let closeTitle = closeDefaultLabel;

    if (headerButtons) {
        if (headerButtons.launchIconDescription) {
            launchLabel = getButtonAriaAttributes(headerButtons.launchIconDescription);
            launchTitle = headerButtons.launchIconDescription;
        }

        if (headerButtons.minimizeIconDescription) {
            minimizeLabel = getButtonAriaAttributes(headerButtons.minimizeIconDescription);
            minimizeTitle = headerButtons.minimizeIconDescription;
        }

        if (headerButtons.maximizeIconDescription) {
            maximizeLabel = getButtonAriaAttributes(headerButtons.maximizeIconDescription);
            maximizeTitle = headerButtons.maximizeIconDescription;
        }

        if (headerButtons.closeIconDescription) {
            closeLabel = getButtonAriaAttributes(headerButtons.closeIconDescription);
            closeTitle = headerButtons.closeIconDescription;
        }
    }

    const launchMarkup =
        `<button id="webchat__header__btn--launch" class="webchat__header__btn" ${launchLabel}>
            <svg ${iconAriaAttributes} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><defs><style>.cls-1{fill:none;}</style></defs><title>${launchTitle}</title><path d="M26,28H6a2,2,0,0,1-2-2V6A2,2,0,0,1,6,4h9V6H6V26H26V17h2v9A2,2,0,0,1,26,28Z"/><polygon points="21 2 21 4 26.59 4 18 12.59 19.41 14 28 5.41 28 11 30 11 30 2 21 2"/><rect id="_Transparent_Rectangle_" data-name="&lt;Transparent Rectangle&gt;" class="cls-1" width="32" height="32"/></svg>
        </button>`;

    const maximizeMarkup =
        `<button id="webchat__header__btn--max" class="webchat__header__btn" ${maximizeLabel}>
            <svg ${iconAriaAttributes} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><defs><style>.cls-1{fill:none;}</style></defs><title>${maximizeTitle}</title><polygon points="21 2 21 4 26.59 4 17 13.58 18.41 15 28 5.41 28 11 30 11 30 2 21 2"/><polygon points="15 18.42 13.59 17 4 26.59 4 21 2 21 2 30 11 30 11 28 5.41 28 15 18.42"/><rect id="_Transparent_Rectangle_" data-name="&lt;Transparent Rectangle&gt;" class="cls-1" width="32" height="32" transform="translate(32 32) rotate(-180)"/></svg>
        </button>`;

    const minimizeMarkup =
        `<button id="webchat__header__btn--min" class="webchat__header__btn webchat__header__btn--hidden" ${minimizeLabel}>
            <svg ${iconAriaAttributes} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><defs><style>.cls-1{fill:none;}</style></defs><title>${minimizeTitle}</title><polygon points="6 17 6 19 11.59 19 2 28.58 3.41 30 13 20.41 13 26 15 26 15 17 6 17"/><polygon points="30 3.42 28.59 2 19 11.59 19 6 17 6 17 15 26 15 26 13 20.41 13 30 3.42"/><rect id="_Transparent_Rectangle_" data-name="&lt;Transparent Rectangle&gt;" class="cls-1" width="32" height="32" transform="translate(32 32) rotate(-180)"/></svg>
        </button>`;

    const closeMarkup =
        `<button id="webchat__header__btn--close" class="webchat__header__btn" ${closeLabel}>
            <svg ${iconAriaAttributes} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><defs><style>.cls-2{fill:none;}</style></defs><title>${closeTitle}</title><polygon class="cls-1" points="24 9.4 22.6 8 16 14.6 9.4 8 8 9.4 14.6 16 8 22.6 9.4 24 16 17.4 22.6 24 24 22.6 17.4 16 24 9.4"/><rect class="cls-2" width="32" height="32"/></svg>
        </button>`;

    return `<div id="webchat__header">${launchMarkup}${maximizeMarkup}${minimizeMarkup}${closeMarkup}</div>`;
}

/**
 * Create dynamically the HTML to display the webchat.
 * @param {String} chatIcon HTML chat button icon.
 * @param {String} tooltip HTML tooltip for the chat button.
 * @param {String} webchatButtonAriaAttributes Aria attributes for the chat button.
 * @param {String} url The url for the webchat iframe
 */
function createHtml({chatButtonIcon, chatButtonTooltip, headerButtons, url}) {
    const div = document.createElement('div');
    document.getElementsByTagName('body')[0].appendChild(div);

    const chatButtonMarkup = getChatButtonMarkup({chatButtonIcon, chatButtonTooltip});
    const headerMarkup = getHeaderMarkup(headerButtons);

    div.outerHTML =
        `<div id="botDiv">
            ${chatButtonMarkup}
            <div
                id="webchat__container"
                class="webchat__container--hidden-on-page-load"
                role="region"
                aria-labelledby="tchat-title"
            >
                <h1 id="tchat-title" class="sr-only">Fenêtre du tchat</h1>
                ${headerMarkup}
                <div id="webchat__content">
                    <iframe id="webchat__iframe" allow="fullscreen" width="400px" height="480px" src="${url}"></iframe>
                </div>
            </div>
        </div>`;
}

/**
 * Set the opening state of the chat window
 * @param {String} state The state of the chat window. `'open'` or `'close`
 * @param {Boolean} memorize memorize the state
 */
function setOpeningState(state, memorize = true) {
    if (!memorize) {
        return;
    }

    sessionStorage.setItem('openingState', state);
}

/**
 * Get the opening state of the chat window
 * @returns {String} return the chat window opening state
 */
function getOpeningState() {
    return sessionStorage.getItem('openingState');
}

/**
 * Handle the opening state of the chat window at script load.
 * @param {Object} opening
 *  - @param {String} opening.mode The opening mode. Can be `'open'`, `'delay'` or `'close'`.
 *  - @param {Number} opening.delay For a delay mode, the delay in milliseconds to open the window.
 *  - @param {Boolean} opening.memorize Memorize the state of opening.
 */
function handleOpeningState(opening) {
    const openingState = getOpeningState();

    if (openingState === 'close' || window.matchMedia('(max-width: 1024px)').matches) {
        return;
    }

    const chatBtn = document.getElementsByClassName('webchat__btn')[0];

    if (!chatBtn) {
        return;
    }

    if (openingState === 'open') {
        chatBtn.click();
    }

    if (!opening) {
        return;
    }

    const {mode, delay} = opening;

    if (!mode || mode === 'close') {
        return;
    }

    if (mode === 'open') {
        chatBtn.click();
    }

    if (mode === 'delay' && delay) {
        setTimeout(function() {
            chatBtn.click();
        }, delay);
    }
}

/**
 * Event listener.
 * Handle chat button click.
 * @param {Node} chatBtn Chat button node.
 * @param {Node} webchat webchat container node.
 */
function handleChatButtonClick(chatBtn, webchat, memorize) {
    chatBtn.addEventListener('click', function(e) {
        chatBtn.classList.add('webchat__btn--hidden');
        chatBtn.classList.remove('webchat__btn--visible-on-page-load');
        chatBtn.classList.remove('webchat__btn--visible');
        webchat.classList.add('webchat__container--opened');
        webchat.classList.remove('webchat__container--hidden-on-page-load');
        webchat.classList.remove('webchat__container--hidden');
        webchat.classList.remove('webchat__container--closed-full-screen');
        webchat.classList.remove('webchat__container--minimized');

        setOpeningState('open', memorize);
    });
}

/**
 * Event listener.
 * Handle chat button key down.
 * @param {Node} chatBtn Chat button node.
 * @param {Node} webchat webchat container node.
 * @param {Boolean} memorize Memorize the opening state.
 */
function handleChatButtonKeyDown(chatBtn, webchat, memorize) {
    chatBtn.addEventListener('keydown', function(e) {
        if (e.keyCode === 13) { // ENTER key
            chatBtn.classList.add('webchat__btn--hidden');
            chatBtn.classList.remove('webchat__btn--visible-on-page-load');
            chatBtn.classList.remove('webchat__btn--visible');
            webchat.classList.add('webchat__container--opened');
            webchat.classList.remove('webchat__container--hidden-on-page-load');
            webchat.classList.remove('webchat__container--hidden');
            webchat.classList.remove('webchat__container--closed-full-screen');
            webchat.classList.remove('webchat__container--minimized');
            setOpeningState('open', memorize);
        }
    });
}

/**
 * Event listener.
 * Handle close header button click.
 * @param {Node} chatBtn Chat button node.
 * @param {Node} closeBtn close button node
 * @param {Node} maxBtn maximize button node
 * @param {Node} minBtn minimize button node.
 * @param {Node} webchat webchat container node.
 * @param {Boolean} memorize Memorize the opening state.
 */
function handleCloseButtonClick(chatBtn, closeBtn, maxBtn, minBtn, webchat, memorize) {
    if (closeBtn) {
        closeBtn.addEventListener('click', function(e) {
            chatBtn.classList.add('webchat__btn--visible');
            chatBtn.classList.remove('webchat__btn--hidden');

            if (webchat.classList.contains('webchat__container--full-screen')) {
                webchat.classList.add('webchat__container--closed-full-screen');
                webchat.classList.remove('webchat__container--full-screen');
            } else {
                webchat.classList.add('webchat__container--hidden');
                webchat.classList.remove('webchat__container--opened');
            }

            minBtn.classList.add('webchat__header__btn--hidden');
            maxBtn.classList.remove('webchat__header__btn--hidden');
            setOpeningState('close', memorize);
        });
    }
}

/**
 * Event listener.
 * Handle launch header button click.
 * @param {Node} launchBtn launch button node.
 * @param {String} url The url of the webchat.
 */
function handleLaunchButtonClick(launchBtn, url) {
    if (launchBtn) {
        launchBtn.addEventListener('click', function(e) {
            window.open(url);
        });
    }
}

/**
 * Event listener.
 * Handle maximize header button click.
 * @param {Node} maxBtn maximize button node
 * @param {Node} minBtn minimize button node.
 * @param {Node} webchat webchat container node.
 */
function handleMaxButtonClick(maxBtn, minBtn, webchat) {
    if (maxBtn) {
        maxBtn.addEventListener('click', function(e) {
            webchat.classList.add('webchat__container--full-screen');
            webchat.classList.remove('webchat__container--opened');
            webchat.classList.remove('webchat__container--minimized');
            minBtn.classList.remove('webchat__header__btn--hidden');
            maxBtn.classList.add('webchat__header__btn--hidden');
        });
    }
}

/**
 * Event listener.
 * Handle minimize header button click.
 * @param {Node} maxBtn maximize button node
 * @param {Node} minBtn minimize button node.
 * @param {Node} webchat webchat container node.
 */
function handleMinButtonClick(maxBtn, minBtn, webchat) {
    if (minBtn) {
        minBtn.addEventListener('click', function(e) {
            webchat.classList.add('webchat__container--minimized');
            webchat.classList.remove('webchat__container--full-screen');
            minBtn.classList.add('webchat__header__btn--hidden');
            maxBtn.classList.remove('webchat__header__btn--hidden');
        });
    }
}

/**
 * Handle DOM events for header buttons & chat button click.
 * @param {String} url The url of the webchat iframe.
 * @param {Boolean} memorize Memorize the opening state.
 */
function handleEventsListener(url, memorize) {
    const webchat = document.querySelector('#webchat__container');
    const chatBtn = document.getElementsByClassName('webchat__btn')[0];
    const launchBtn = document.querySelector('#webchat__header__btn--launch');
    const maxBtn = document.querySelector('#webchat__header__btn--max');
    const minBtn = document.querySelector('#webchat__header__btn--min');
    const closeBtn = document.querySelector('#webchat__header__btn--close');

    handleChatButtonClick(chatBtn, webchat, memorize);
    handleChatButtonKeyDown(chatBtn, webchat, memorize);
    handleCloseButtonClick(chatBtn, closeBtn, maxBtn, minBtn, webchat, memorize);
    handleLaunchButtonClick(launchBtn, url);
    handleMaxButtonClick(maxBtn, minBtn, webchat);
    handleMinButtonClick(maxBtn, minBtn, webchat);
}

/**
 * Create embedded webchat window.
 * @param {Object} params
 * @param {Object} params.chatButtonIcon Customize the icon of the chat button.
 *  - @param {String} params.chatButtonIcon.url The url of the chat button icon.
 *  - @param {String} params.chatButtonIcon.altText The alternative text of the chat button icon.
 *  - @param {String} params.chatButtonIcon.height The height of the chat button icon.
 *  - @param {String} params.chatButtonIcon.width The width of the chat button icon.
 *  - @param {String} params.chatButtonIcon.borderRadius The border radius of the chat button icon.
 * @param {String} params.chatButtonTooltip The chat button tooltip description.
 *  - @param {String} params.chatButtonTooltip.text The chat button tooltip text.
 *  - @param {String} params.chatButtonTooltip.backgroundColor The chat button tooltip background color.
 *  - @param {String} params.chatButtonTooltip.textColor The chat button tooltip text color.
 *  - @param {String} params.chatButtonTooltip.visibility The chat button tooltip visibility.
 * @param {String} params.color The color of the speech bubble button and of the header.
 * @param {Object} params.headerButtons Customize the buttons of the webchat window header.
 *  - @param {String} params.headerButtons.color Customize the color of the webchat window header.
 *  - @param {String} params.headerButtons.closeIconDescription Customize the aria label of the close icon button.
 *  - @param {String} params.headerButtons.launchIconDescription Customize the aria label of the launch icon button.
 *  - @param {String} params.headerButtons.maximizeIconDescription Customize the aria label of the maximize icon button.
 *  - @param {String} params.headerButtons.minimizeIconDescription Customize the aria label of the minimize icon button.
 * @param {String} params.height The height of the chat window.
 * @param {String} params.width The width of the chat window.
 * @param {String} params.opening The opening of the chat window after page load.
 *  - @param {String} params.opening.mode The opening mode. Can be `'open'`, `'delay'` or `'close'`.
 *  - @param {Number} params.opening.delay For a delay mode, the delay in milliseconds to open the window.
 *  - @param {Boolean} params.opening.memorize Memorize the state of opening.
 * @param {Object} params.position The position of the chat button in the document.
 *  - @param {Object} params.position.right The position of the chat button and in the document.
 *  - @param {Object} params.position.bottom The position of the chat button and in the document.
 * @param {Object} params.webchatParams The url params.
 *  - @param {String} params.webchatParams.userId the id of the user.
 *  - @param {String} params.webchatParams.userIdType the type of id. Can be email or login.
 *  - @param {String} params.webchatParams.userFirstName the first name of the user.
 *  - @param {String} params.webchatParams.userLastName the last name of the user.
 *  - @param {String} params.webchatParams.webChatToken the webchat token.
 */
function wrapWebchat(params) {
    // Get the url of the iframe.
    const url = getUrl(params.webchatParams);

    // Create css stylesheet dynamically.
    addStylesheetRules();

    // Custom CSS style in function of given parameters.
    customizeStyle({
        chatButtonIcon: params.chatButtonIcon,
        chatButtonTooltip: params.chatButtonTooltip,
        color: params.color,
        headerButtons: params.headerButtons,
        height: params.height,
        position: params.position,
        width: params.width,
    });

    // Create webchat window HTML markup.
    createHtml({
        chatButtonIcon: params.chatButtonIcon,
        chatButtonTooltip: params.chatButtonTooltip,
        headerButtons: params.headerButtons,
        url,
    });

    const memorize = params.opening ? params.opening.memorize : true;

    // Handle DOM events
    handleEventsListener(url, memorize);

    // Handle the opening state of the chat window at page load.
    handleOpeningState(params.opening);
}
